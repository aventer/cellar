<?php
/**
 * Created by JetBrains PhpStorm.
 * User: nomad
 * Date: 2012/11/28
 * Time: 10:29 PM
 * To change this template use File | Settings | File Templates.
 */
class Utilities
{
    var $objectsList;

    function _construct(){

    }

    public function convertMultiplePhpActiveRecordObjectsToArray($objectsList){

        $complexArray = array();

        $this->objectsList = $objectsList;

        foreach ($this->objectsList as &$result) {
            $complexArray [] = (array)$result->to_array();
        }

        return $complexArray;
    }

}
