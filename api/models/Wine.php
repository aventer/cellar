<?php
/**
 * Created by JetBrains PhpStorm.
 * User: nomad
 * Date: 2012/11/28
 * Time: 9:46 PM
 * To change this template use File | Settings | File Templates.
 */

class Wine extends ActiveRecord\Model
{
    static $table_name = 'wine'; // this is an explicit definition of the table name because it does not follow the plural convention "wines"

}