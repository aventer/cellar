(function () {

    Wine = can.Model({
        findAll:'GET /cellar/api/wines'
    }, {});

    Wine.List = can.Model.List({
        filter: function(wine){
            this.attr('length');
            var wines = new Wine.List([]);
            this.each(function(wine, i){
                    wines.push(wine)
            })
            return wines;
        },
        count: function(wine) {
            return this.filter(wine).length;
        }
    });

    Wines = can.Control({
        init:function(){
            this.element.html(can.view('views/winesList.ejs', {
                wines:this.options.wines
            }));
        }
    });

    $(document).ready(function () {
        $.when(Wine.findAll().then(function (wineResponse) {
            //var wines = wineResponse[0] == null ? [] : (wineResponse[0].wine instanceof Array ? wineResponse[0].wine : [wineResponse[0].wine]);
            var wines = wineResponse[0].wine;
            new Wines('#wines', {
                wines:wines

            });
            console.log(wines);
        }));
    });

})();